﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDscript : MonoBehaviour {

	float playerScore = 0;
	public Text scoreText;

	void Start () 
	{
		SetScoreText ();
	}

	void Update ()
	{
		SetScoreText ();
	}

	public void IncreaseScore(int amount)
	{
		playerScore += amount; 
	}

	public void DecreaseScore(int amount)
	{
		playerScore -= amount; 
	}

	void SetScoreText ()
	{
		scoreText.text = "Score: " + playerScore;
	}

	public int GetScore()
	{
		return (int)playerScore;
	}

	void OnDisable()
	{
		PlayerPrefs.SetInt ("Score", (int)(playerScore));
	}
}
