﻿using UnityEngine;
using System.Collections;

public class TokenScript : MonoBehaviour {

	HUDscript hud;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			hud = GameObject.Find ("Main Camera").GetComponent<HUDscript> ();
			hud.IncreaseScore (1);
			Destroy (this.gameObject);
		}
	}
}
