﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

	HUDscript hud;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			hud = GameObject.Find ("Main Camera").GetComponent<HUDscript> ();
			hud.DecreaseScore (1);
			if (hud.GetScore() < 0) 
			{
				Application.LoadLevel (3);
				return;
			}
			Destroy (this.gameObject);
		}
	}
}
