﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverScript : MonoBehaviour {

	int score = 0;
	public Text GOText;
	public Text scoreText;

	void Start () 
	{
		score = PlayerPrefs.GetInt ("Score");
		GOText.text = "Game Over";
		SetScoreText ();
	}

	void SetScoreText ()
	{
		scoreText.text = "Score: " + score;
	}

	void OnGUI()
	{
		if (GUI.Button (new Rect (Screen.width / 2 - 60, 350, 80, 30), "Main Menu")) {
			Application.LoadLevel (0);
		}
	}

}
