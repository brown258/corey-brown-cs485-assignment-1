﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

	public Text MMText;

	void Start () 
	{
		MMText.text = "Main Menu";
	}

	void OnGUI()
	{
		if (GUI.Button (new Rect (Screen.width / 2 - 60, 200, 80, 30), "Roll-A-Ball")) {
			Application.LoadLevel (1);
		}
		if (GUI.Button (new Rect (Screen.width / 2 - 60, 250, 120, 30), "Impossible Game")) {
			Application.LoadLevel (2);
		}
	}
}
