﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RollABall_PlayerController : MonoBehaviour {

	public float speed;
	public Text scoreText;
	public Text winText;

	private Rigidbody rb;
	private int score;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		score = 0;
		SetscoreText ();
		winText.text = "";

	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ( "Pick Up"))
		{
			other.gameObject.SetActive (false);
			score = score + 1;
			SetscoreText ();
		}
	}

	void SetscoreText ()
	{
		scoreText.text = "Score: " + score.ToString ();
		if (score >= 12)
		{
			Application.LoadLevel (3);
			winText.text = "You Win!";
		}
	}

	void OnDisable()
	{
		PlayerPrefs.SetInt ("Score", (int)(score));
	}

}
