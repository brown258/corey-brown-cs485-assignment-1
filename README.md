# Corey Brown #
### cs485 Game Programming Assignment 1 ###

### Assignment 1 Report ###

### Brief Introduction of the game ###

There is a main menu.  You can choose between the Roll-A-Ball game or the Impossible Game. The Impossible Game is an infinite game that is meant to assess the player's ability to improve their score each time they play much like the famous mobile app flappy bird.  Jump onto the platforms and collect yellow spinning tokens along the way to add to your score.  Collide into an enemy box and it will decrement your score.  If you collide into it and it results in a negative score or you fall off the screen then it is Game Over.  At the Game Over screen, it will prompt you to return to the main menu where you can choose to play again or a different game. 

### Instructions of playing the game ###
Main Menu
Use your mouse to click on the game you would like to play

Roll-A-Ball
Use the arrow keys to roll the ball 
Collect all of the tokens to win the game

Impossible Game
Hit the spacebar to jump… Twice to double jump.
Collect as many tokens as you can by colliding into them
Avoid the red enemy boxes
Colliding into an enemy will decrement your score
Colliding into an enemy that results in a negative score and it’s game over

### References ### 
Roll-A-Ball Tutorial
https://unity3d.com/learn/tutorials/projects/roll-ball-tutorial

Infinite Runner Tutorial
https://www.youtube.com/watch?v=7dP7R-GbFkM

Standard Assets for Unity 4.6
https://www.assetstore.unity3d.com/en/#!/content/21064

### My own contributions ### 
Added a Main Menu
Added the token feature I learned from the Roll-A-Ball tutorial
Added the UI I learned from the Roll-A-Ball tutorial
Added an “enemy” that will decrement your score and result in a Game Over if the score is negative
Modified the architecture

### Lessons Learned ###

I learned a lot about OnTrigger functions, spawning scripts, how to make a general UI.  I learned how to make a script so the camera follows the player.  I learned how to properly use prefabs.  I learned a lot of the basics it takes to make a simple game.  I just want to learn to get better at writing Unity code because I looked into the Scripting API reference and there is a lot to learn.  There’s so much information on the Unity website that it is overwhelming but I hope to gain much knowledge throughout the semester.  

### Future Improvements on the game ### 
I would have wanted the look of the tokens and enemies to be more graphical.  I would want to make it 3D and once I know how, to make the character a person and shoot the enemies.  I would love to make a Super Mario kind of game and that is what came to mind when making this.  